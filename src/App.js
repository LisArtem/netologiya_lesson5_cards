import React from 'react';
// import logo from './logo.svg';
import './App.css';
import Card from './Card'
import CardNew from './CardNew'

function App(props) {


    return (
        <div>
            <Card/>
            <CardNew>
                <img src="Image.png" className="card-img-top" alt="#"/>
                <div className="card-body">
                    <h5 className="card-title">Special title treatment</h5>
                    <p className="card-text">With supporting text below as a natural lead-in to additional content.</p>
                    <a href="#" className="btn btn-primary">Go somewhere</a>
                </div>
            </CardNew>
        </div>

    );
}

export default App;
