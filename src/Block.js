import React from 'react';
import Image from './Image'

export default function Block (props) {

    return (
        <div>
            <div>
                <div className="card">
                    <Image />
                    <div className="card-body">
                        <h5 className="card-title">{props.title}</h5>
                        <p className="card-text">{props.text}</p>
                        <a href="#" className="btn btn-primary">{props.buttontext}</a>
                    </div>
                </div>
            </div>
        </div>
    );
}