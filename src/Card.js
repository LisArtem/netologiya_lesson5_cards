import React from 'react'
import Block from './Block'


function Card(props) {

    const info = {
        title: 'Card title',
        text: 'Some quick example text to build on the card title and make up the bulk of the card\'s content.',
        buttontext: 'Go somewhere'
    }

    return (
        <>
        <Block {...info} />
        </>
    );
}

export default Card;